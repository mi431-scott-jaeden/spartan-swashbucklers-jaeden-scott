#define NAV_PATH_V2

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using XnTools;



[CreateAssetMenu( fileName = "CS_JGB_Merchant_SO",
    menuName = "ScriptableObjects/Captain Strategies/CS_JGB_Merchant_SO (Old Version)", order = 1 )]
public class CS_JGB_Merchant_SO : CaptainStrategy_SO {
    // NOTE: This is assigned by SeaMap for the Merchants, but you can populate it as you travel around - JGB 2023-04-02
    static private List<Vector2Int> _SETTLEMENTS;
    
    [SerializeField] private int   minPercentOfMapToTraverse = 30;
    [SerializeField] private bool  useDiagonalNavigation     = true;
    [SerializeField] private float navPointIsCloseEnough     = 10;
    [SerializeField] private int   varianceOnStraightLines   = 20;


    [SerializeField]
#if NAV_PATH_V2
    private List<Vector3> navPath;
#else    
    private List<AStarComparable> navPath;
#endif
    private AStarSeaMapPathfinder pathfinder;
    private bool                  plotNavPathCalled = false;

    public override void InitCaptain() {
        // Clear the old navigation data.
        plotNavPathCalled = false;
        navPath = new List<Vector3>();
    }

    public override void CleanUpCaptain() {
        base.CleanUpCaptain();
        if ( _selfInfo.health <= 0 ) {
            MerchantManager.ShipSank( _selfInfo );
        }
    }
    
    protected override void SetCaptainName() {
        captainName = "CS_JGB_Merchant_SO";
    }

    private SenseInfo _selfInfo;
    
    /// <summary>
    /// <para>This function is called every FixedUpdate, and it is the only way that your Captain
    ///  can give commands to the ship. See CS_JGB_Merchant_SO and CS_HumanPlayer_SO for examples.</para>
    /// <para>Commands include:</para>
    /// <para>none - Do nothing</para>
    /// <para>sailsUp - Raise the sails to Accelerate</para>
    /// <para>sailsDown - Drop the sails to Decelerate</para>
    /// <para>turnLeft - Turn left, decreasing the heading</para>
    /// <para>turnRight - Turn right, increasing the heading</para>
    /// <para>turnToHeading, float toHeading – Turn toward toHeading</para>
    /// <para>fire - Fire all cannon pairs randomly (only the starboard or port cannon of each pair will fire)</para>
    /// <para>fireAtPoint, Vector3 aimPoint - Attempt to aim cannon at a specific point in world space. Cannon aim angle is limited by SETTINGS</para>
    /// </summary>
    public override List<Ship.Command> AIUpdate(SenseInfo selfInfo, List<SenseInfo> sensed) {
        // Ship.eCommands can be found in the Ship script and include:
        
        commands.Clear(); // These will also be cleared by the Ship as it attempts them.
        
        _selfInfo = selfInfo;
        if(!plotNavPathCalled) PlotNavPath();
        if ( navPath == null || navPath.Count == 0 ) {
            // Just wait for the AStar to come back
            return commands;
        }
        
        // Determine whether we're close enough to move to the next point on the path
#if NAV_PATH_V2
        Vector3 nextNavPoint = navPath[0];
#else
        Vector3 nextNavPoint = navPath[0].position;
#endif
        if ( ( nextNavPoint - selfInfo.position ).magnitude <= navPointIsCloseEnough ) {
            navPath.RemoveAt( 0 );
            // Are we done?
            if ( navPath.Count == 0 ) {
                MerchantManager.ShipArrived( selfInfo );
                commands.Add(new Ship.Command(Ship.eCommand.sailsDown)  );
                return commands;
            }
            // Otherwise, head to the new nav point
#if NAV_PATH_V2
            nextNavPoint = navPath[0];
#else            
            nextNavPoint = navPath[0].position;
#endif
        }

        Vector3 deltaToNavPoint = nextNavPoint - selfInfo.position;
        float desiredHeading = SeaMap.DirectionToHeading( deltaToNavPoint );
        commands.Add( new Ship.Command( Ship.eCommand.turnToHeading, desiredHeading ) );
        if ( !selfInfo.sailsUp ) commands.Add( new Ship.Command( Ship.eCommand.sailsUp ) );

        return commands;
    }

    /// <summary>
    /// Find a port that is a significant distance away and plot a path to it using A*
    /// </summary>
    void PlotNavPath() {
        // Choose a start and end point
        Vector2Int startMapLoc = _selfInfo.mapLoc, endMapLoc;
        int iterationCount = 0;
        int minPathHCost = SeaMap.TEXTURE_RESOLUTION * minPercentOfMapToTraverse / 100;
        do {
            endMapLoc = _SETTLEMENTS[_SETTLEMENTS.RandomIndex()];
            iterationCount++;
        } while ( (iterationCount < 100) && (AStarSeaMapPathfinder.GetHCost( startMapLoc, endMapLoc, useDiagonalNavigation ) < minPathHCost) );

        pathfinder = new AStarSeaMapPathfinder();
        GameManager.START_COROUTINE(pathfinder.PathFindCoRo(startMapLoc, endMapLoc, useDiagonalNavigation, PathFound));
        plotNavPathCalled = true;
    }

    void PathFound( List<AStarComparable> foundPath ) {
        pathfinder = null; // Release the memory for pathfinder
        if ( foundPath == null || foundPath.Count < 2 ) {
            plotNavPathCalled = false; // Ask for another path
            return;
        }
        
#if NAV_PATH_V2
        float[] pathLerpAmounts = new[] { Random.Range(0.35f,0.45f), 0.5f, Random.Range(0.55f,0.65f) };
        navPath = new List<Vector3>();
        Vector3 p0, p1, n0, n1, n2, thisDir, lastDir = Vector3.zero;
        float randy = Random.Range( -varianceOnStraightLines, varianceOnStraightLines );
        for ( int i = 1; i < foundPath.Count; i++ ) {
            p0 = foundPath[i - 1].position;
            p1 = foundPath[i].position;
            n0 = Vector3.Lerp( p0, p1, pathLerpAmounts[0] );
            n1 = Vector3.Lerp( p0, p1, pathLerpAmounts[1] );
            n2 = Vector3.Lerp( p0, p1, pathLerpAmounts[2] );
            thisDir = ( n2 - n0 );//.normalized;
            if ( n0.x.IsApproximately(n1.x, 1) && n1.x.IsApproximately(n2.x,1) ) {
                // It's straight in the x dimension, so it can be varied
                n0.x += randy;
                n1.x += randy;
                n2.x += randy;
            } else if ( n0.z.IsApproximately(n1.z, 1) && n1.z.IsApproximately(n2.z,1) ) {
                // It's straight in the x dimension, so it can be varied
                n0.z += randy;
                n1.z += randy;
                n2.z += randy;
            } else if ( ( thisDir - lastDir ).magnitude < 1f ) {
                n0.x += randy;
                n1.x += randy;
                n2.x += randy;
                n0.z -= randy;
                n1.z -= randy;
                n2.z -= randy;
            }
            lastDir = thisDir;
            navPath.AddRange( n0, n1, n2 );
            // for ( int j = 0; j < pathLerpAmounts.Length; j++ ) {
            //     navPath.Add( Vector3.Lerp( p0, p1, pathLerpAmounts[j] ) );
            // }
        }
#else        
        // The 0th element is where we already are, so no need to navigate there
        navPath.RemoveAt( 0 );
        navPath = foundPath;
#endif        
    }

    private Color col = Color.clear;
    public override void OnDrawGizmos() {
        if ( navPath != null && navPath.Count > 0 ) {
            if ( col == Color.clear ) {
                col = Random.ColorHSV( 0, 1, 1, 1, 1, 1 );
            }
            Vector3 offset = Vector3.up * 2;
            Gizmos.color = col;
#if NAV_PATH_V2
            Gizmos.DrawLine( _selfInfo.position+offset, navPath[0]+offset );
            for ( int i = 1; i < navPath.Count; i++ ) {
                Gizmos.DrawLine( navPath[i - 1] +offset, navPath[i]+offset );
            }
#else            
            Gizmos.DrawLine( _selfInfo.position+offset, navPath[0].position+offset );
            for ( int i = 1; i < navPath.Count; i++ ) {
                Gizmos.DrawLine( navPath[i - 1].position+offset, navPath[i].position+offset );
            }
#endif
        }
    }


    static public void ASSIGN_SETTLEMENTS( List<Vector2Int> tList ) {
        _SETTLEMENTS = tList;
    }
}