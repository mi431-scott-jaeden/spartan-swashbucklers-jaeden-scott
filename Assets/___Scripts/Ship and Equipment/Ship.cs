using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using UnityEngine;
using NaughtyAttributes;
using XnTools;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Boatswain))]
[RequireComponent(typeof(MasterGunner))]
[DefaultExecutionOrder(-10)]
public class Ship : MonoBehaviour {
    
#region PUBLIC_REGION
    public enum eCommand {
        none,
        sailsUp,
        sailsDown,
        turnLeft,
        turnRight,
        turnToHeading,
        fire,
        fireAtPoint,
        destroySelf
    }
    
    public class Command {
        public eCommand type  { get; private set; }
        public Vector3  point { get; private set; }
        public float    num   { get; private set; }
        
        /// <summary>
        /// Use this to issue any commands that don't require a parameter.
        /// </summary>
        /// <param name="tType">sailsUp, sailsDown, turnLeft, turnRight, fire, or destroySelf</param>
        public Command( eCommand tType ) {
            type = tType;
        }

        /// <summary>
        /// Use this to issue a fireAtPoint command.
        /// </summary>
        /// <param name="tType">fireAtPoint</param>
        /// <param name="tPoint">The point that the cannon should aim for. Note that the y value is ignored.</param>
        public Command( eCommand tType, Vector3 tPoint ) {
            type = tType;
            point = tPoint;
        }

        /// <summary>
        /// Use this to issue a turnToHeading command.
        /// </summary>
        /// <param name="tType">turnToHeading</param>
        /// <param name="tPoint">The heading (in degrees) that the ship should turn toward. 0 is North, and 90 is East.</param>
        public Command( eCommand tType, float tNum ) {
            type = tType;
            num = tNum;
        }
    }
#endregion
    
    
    static private int _SHIP_ID = 0;
    static private int NEXT_SHIP_ID {
        get {
            return ( _SHIP_ID++ );
        }
    }

    public bool       thisIsANonSpawnedTestShip = false;
    
    public eShipClass shipClass                 = eShipClass.swashbuckler;
    [Expandable]
    public CaptainStrategy_SO captainSO;
    private List<Command> commands = new List<Command>();
    
    [Foldout("Ship's Components and Crew")][SerializeField] Rigidbody       rigid;
    [Foldout("Ship's Components and Crew")][SerializeField] Boatswain       bosun;
    [Foldout("Ship's Components and Crew")][SerializeField] MasterGunner    gunny;
    [Foldout("Ship's Components and Crew")][SerializeField] ShipStatDisplay stats;
    [Foldout("Ship's Components and Crew")][SerializeField] ShipStatsScaler scaly;
    // [Foldout("Ship's Components and Crew")][SerializeField] SenseInfoReporter sIR;
    // [Foldout("Ship's Components and Crew")][SerializeField]  Navigator    navi;
    // [Foldout("Ship's Components and Crew")][SerializeField] Carpenter carp;
    // [Foldout("Ship's Components and Crew")][SerializeField] Quartermaster quart;

    // [Foldout( "Ship's Components and Crew" )]
    [Button]
    private void PopulateShipsComponents() {
        rigid = GetComponent<Rigidbody>();
        bosun = GetComponent<Boatswain>();
        gunny = GetComponent<MasterGunner>();
        gunny.SetShip(this);
        stats = GetComponentInChildren<ShipStatDisplay>();
        scaly = GetComponentInChildren<ShipStatsScaler>();
        // sIR = GetComponent<SenseInfoReporter>();
        // navi = GetComponent<Navigator>();
    }

    [BoxGroup("Set by Spartan Swashbuckler Settings")] public float maxSpeed = 20;
    [BoxGroup("Set by Spartan Swashbuckler Settings")] public float speedEasingPerSecond = 0.25f;
    [BoxGroup("Set by Spartan Swashbuckler Settings")] public float baseTurnRatePerSecond = 60;
    [BoxGroup("Set by Spartan Swashbuckler Settings")] public float turnBleedSpeedMultiplier = 2;

    [BoxGroup("Dynamic")]   public int   shipID   = 0;
    [BoxGroup("Dynamic")]   public float speed    = 0;
    [BoxGroup("Dynamic")]   public float turning  = 0;
    [BoxGroup("Dynamic")]   public float _heading = 0;
    [BoxGroup("Dynamic")]   public float health   = 0;
    [BoxGroup("Dynamic")]   public float crew     = 0;
    [BoxGroup("Dynamic")]   public float cannon   = 0;
    [BoxGroup("Dynamic")]   public float loot     = 0;
    // [BoxGroup("Dynamic")] public float heading = 0;
    // [BoxGroup("Ship's Log")]
    // public XnLog shipsLog;

    public float healthMax     => sst.healthMax;
    public float healthPercent => health / sst.healthMax;
    
    public  Vector2Int mapLoc { get; private set; }
    private Vector2Int lastMapLoc = new Vector2Int( -1, -1 );

    private void Awake() {
        shipID = NEXT_SHIP_ID;
        SetShipValuesFromSwashbucklerSettings();
        SeaMap.SEA_MAP_UPDATE += SeaMapUpdate;
        GameManager.RegisterForAISlot( this );
    }

    private void Start() {
        if ( shipClass == eShipClass.swashbuckler ) {
            MiniMap.TRACK_ON_MAP( MiniMap.eMapTrackerType.swashbuckler, transform );
        } else {
            MiniMap.TRACK_ON_MAP( MiniMap.eMapTrackerType.merchant, transform );
        }
        if ( captainSO != null ) {
            if ( thisIsANonSpawnedTestShip ) {
                if ( GameManager.SETTINGS.captainRecDict.ContainsKey( captainSO ) ) {
                    GameManager.SETTINGS.captainRecDict[captainSO].currentShip = this;
                }
            }
            captainSO.PreInitCaptain();
            captainSO.InitCaptain();
        }
        gunny.SetCaptain( captainSO );
        
        
        // scaly.shipStatsTransform = stats.transform;
        // Color pri, sec;
        // // Using a Tuple to return two values from a method!
        // ( pri, sec ) = GameManager.SETTINGS.GetCaptainColors( captainSO );
        // scaly.SetColors(pri, sec);
        // scaly.enabled = false;
    }

    private void OnDestroy() {
        captainSO.CleanUpCaptain();
        captainSO.PostCleanUpCaptain();
        SeaMap.SEA_MAP_UPDATE -= SeaMapUpdate;
        SeaMap.REMOVE_SHIP( this, mapLoc, lastMapLoc );
        MiniMap.REMOVE_FROM_MAP( transform );
        GameManager.SHIP_DIED(this, captainSO);
    }

    internal ShipSettings sst;
    void SetShipValuesFromSwashbucklerSettings() {
        sst = GameManager.SETTINGS.shipSettingsDict[shipClass];
        maxSpeed = sst.maxSpeed;
        speedEasingPerSecond = sst.speedEasingPerSecond;
        baseTurnRatePerSecond = sst.baseTurnRatePerSecond;
        turnBleedSpeedMultiplier = sst.turnBleedSpeedMultiplier;
        health = sst.healthMax;
        crew = sst.crewMax;
        cannon = 0; // Cannon will begin reloading immediately.
        loot = Random.Range( sst.minMaxLootX100[0], sst.minMaxLootX100[1] + 1 ) * 100;
    }

    public bool GetSailsUp() {
        return bosun.sailsUp;
    }
    // public void SailsUp() { bosun.sailsUp = true; }
    // public void SailsDown() { bosun.sailsUp = false; }

    public void Turn( int direction ) {
        turning = direction;
    }
    public float heading {
        get { return _heading; }
        private set { _heading = value; }
    }

    private float desiredHeading;
    
    public void TurnToHeading( float degrees ) {
        while ( heading - degrees > 180 ) {
            degrees += 360;
        }
        while (degrees - heading > 180) {
            degrees -= 360;
        }
        // If we're within 1 degree of the desiredHeading, don't worry about it.
        if ( heading.IsApproximately( desiredHeading, 1 ) ) {
            desiredHeading = float.NaN;
            return;
        }
        desiredHeading = degrees;
        if ( degrees      > heading ) turning = 1;
        else if ( degrees < heading ) turning = -1;
    }

#region SeaMapUpdate
    /// <summary>
    /// Called during SeaMap.FixedUpdate() to set up all the proper information for Ship.FixedUpdate() to call captainSO.AIUpdate()
    /// </summary>
    void SeaMapUpdate() {
        RepairAndReload();
        GenerateSenseInfos();
    }

    void RepairAndReload() {
        if ( health <= 0 || crew <= 0) {
            DestroyShip();
            return;
        }
        
        lastMapLoc = mapLoc;
        mapLoc = SeaMap.MapLoc( transform.position );
        mapLoc = SeaMap.WRAP_MAP( mapLoc, transform ); // Wrap screen if needed
        if (mapLoc != lastMapLoc) SeaMap.MOVE_SHIP(this, lastMapLoc);
        ShipSettings sst = GameManager.SETTINGS.shipSettingsDict[shipClass];
        float crewMultiplier = crew * 0.01f;
        crewMultiplier = 1 - ( ( 1 - crewMultiplier ) * ( 1 - crewMultiplier ) );
        health = Mathf.Min( health + ( sst.healRate * crew / 100f * Time.fixedDeltaTime ), sst.healthMax );
        cannon = Mathf.Min( cannon + ( sst.reloadRate * crew / 100f * Time.fixedDeltaTime ), sst.cannonMax );
        ShipDoctor.AttemptHeal( this, mapLoc );
        // Damage Per Second for passing over land
        if ( !PublicInfo.NAVIGABLE[mapLoc.x, mapLoc.y] ) {
            health -= GameManager.SETTINGS.landDPS * Time.fixedDeltaTime;
        }
        
        // Attempt banking money
        if ( loot > 0 && shipClass == eShipClass.swashbuckler ) {
            GameManager.AttemptBankMoney(this, mapLoc);
        }
    }

    void DestroyShip() {
        Debug.LogWarning( $"{captainSO.captainName} shipID {shipID} died! Health: {health:0} Crew: {crew:0}" );
            
        // GameManager will dandle distribution of wealth
        GameManager.DistributeLoot( this, damageDict );
        if ( shipClass != eShipClass.swashbuckler ) {
            MerchantManager.ShipSank(this);
        }
        Destroy( gameObject );
    }

    public bool CheckDamageDict( Dictionary<CaptainStrategy_SO, float> testDict ) {
        return ( testDict == damageDict );
    }
    
    

    private SenseInfo[] senseInfos;
    private bool[]      senseInfosSet;
    
    void GenerateSenseInfos() {
        if ( senseInfos == null ) { senseInfos = new SenseInfo[SpartanSwashbucklersSettings_SO.MAP_SENSE_MAX_DISTANCE_PLUS_1]; }
        senseInfosSet = new bool[SpartanSwashbucklersSettings_SO.MAP_SENSE_MAX_DISTANCE_PLUS_1];
        // Commented out because these will only be generated if needed - JGB 2023-04-01
        // for ( int i = 0; i < SpartanSwashbucklersSettings_SO.MAP_SENSE_MAX_DISTANCE_PLUS_1; i++ ) {
        //     senseInfos[i] = new SenseInfo( i, this );
        // }
    }

    public SenseInfo GetSenseInfoAtDistance( int distance ) {
        if ( distance > SpartanSwashbucklersSettings_SO.MAP_SENSE_MAX_DISTANCE_PLUS_1 ) return new SenseInfo();
        if ( senseInfosSet[distance] ) return senseInfos[distance];
        // The SenseInfo at that distance must be created
        senseInfos[distance] = new SenseInfo( distance, this );
        senseInfosSet[distance] = true;
        return senseInfos[distance];
    }
#endregion

    
    void FixedUpdate() {
        if ( GameManager.IsItMyTurn( this ) ) {
            if ( captainSO != null ) {
                // Clear the commands from the last AIUpdate
                commands.Clear();
                commands = captainSO.AIUpdate( GetSenseInfoAtDistance( 0 ), SeaMap.GET_SENSE_INFOS_AROUND( this ) );
            }
        }
        ExecuteCommands();
        
        float uAcc = speedEasingPerSecond * Time.deltaTime;
        if (bosun.sailsUp) {
            // Accelerate
            speed = Mathf.Lerp(speed, maxSpeed, uAcc);
        } else {
            speed = Mathf.Lerp(speed, 0, uAcc);
        }

        if (turning != 0) {
            float turnRate = baseTurnRatePerSecond * Time.deltaTime;
            if ( !float.IsNaN( desiredHeading ) &&
                 ( Mathf.Abs( heading - desiredHeading ) < turnRate ) ) {
                // This should stop the jitter of merchant ships.
                heading = desiredHeading;
            } else {
                heading += turning * turnRate;
            }
            if ( heading < 0 ) heading += 360;
            else if ( heading >= 360 ) heading -= 360;
            
            // Turning bleeds speed
            speed = Mathf.Lerp(speed, 0, uAcc*turnBleedSpeedMultiplier);
        }
        
        transform.rotation = Quaternion.Euler(0, heading, 0);
        rigid.velocity = speed * transform.forward;
    }
    
    

    void ExecuteCommands() {
        turning = 0;
        if ( commands == null || commands.Count == 0 ) return;
        foreach ( Ship.Command command in commands ) {
            switch ( command.type ) {
            case eCommand.sailsUp:
                bosun.sailsUp = true;
                break;
            case eCommand.sailsDown:
                bosun.sailsUp = false;
                break;
            case eCommand.turnLeft:
                desiredHeading = float.NaN;
                turning += -1;
                break;
            case eCommand.turnRight:
                desiredHeading = float.NaN;
                turning += 1;
                break;
            case eCommand.turnToHeading:
                TurnToHeading( command.num );
                break;
            case eCommand.fire:
                if ( cannon >= 1 ) {
                    gunny.Fire( Mathf.FloorToInt( cannon ) );
                    cannon = 0;
                }
                break;
            case eCommand.fireAtPoint:
                if ( cannon >= 1 ) {
                    gunny.FireAtPoint( Mathf.FloorToInt( cannon ), command.point );
                    cannon = 0;
                }
                break;
            default:
                // do nothing
                break;
            }
        }
        
        // // Clear the commands from this frame
        // commands.Clear();
        // Commands are no longer cleared because of the GameManager._AI_SLOTS. - JGB 2023-04-22
    }


    void OnDrawGizmos() {
        if ( Application.isPlaying && Application.isEditor ) {
            // Only draw if we're in editor and the application is playing
            if ( captainSO != null ) captainSO.OnDrawGizmos();
        }
    }
    void OnDrawGizmosSelected() {
        if ( Application.isPlaying && Application.isEditor ) {
            // Only draw if we're in editor and the application is playing
            if ( captainSO != null ) captainSO.OnDrawGizmosSelected();
        }
    }

    /// <summary>
    /// Calculates the acceleration % for a given heading.
    /// TODO: Return a number other than 1
    /// </summary>
    /// <param name="heading"></param>
    /// <returns></returns>
    float CalculateWind(float heading) {
        return 1;
    }

#region Cannonball Damage
    private void OnCollisionEnter( Collision collision ) {
        if (health == sst.healthMax) damageDict.Clear();
        
        Cannonball cb = collision.gameObject.GetComponent<Cannonball>();
        if ( cb == null ) {
            Debug.LogWarning( $"Something other than a Cannonball collided with a Ship. It was a {gameObject.name}, {gameObject}" );
            return;
        }
        health -= Cannonball.DAMAGE;
        if ( !damageDict.ContainsKey( cb.captainSO ) ) {
            damageDict.Add(cb.captainSO, Cannonball.DAMAGE);
        } else {
            damageDict[cb.captainSO] += Cannonball.DAMAGE;
        }
    }

    private Dictionary<CaptainStrategy_SO, float> damageDict = new Dictionary<CaptainStrategy_SO, float>();

#endregion

}
