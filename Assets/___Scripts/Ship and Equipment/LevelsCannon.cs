using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class LevelsCannon : MonoBehaviour {
    [Range( 0, 3 )]
    public int level = 0;
    public List<CannonGroup> groups;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void ShowLevel() {
        for ( int i = 0; i < groups.Count; i++ ) {
            if ( i <= level ) {
                foreach ( Cannon c in groups[i].items ) {
                    c.isOn = true;
                }
            } else {
                foreach ( Cannon c in groups[i].items ) {
                    c.isOn = false;
                }
            }
        }
    }

#if UNITY_EDITOR
    private void OnValidate() {
        ShowLevel();
    }
#endif

    [System.Serializable]
    public class CannonGroup {
        [FormerlySerializedAs( "cannon" )]
        public List<Cannon> items;
    }
}
