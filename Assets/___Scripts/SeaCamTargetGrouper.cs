using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using XnTools;
using Target = Cinemachine.CinemachineTargetGroup.Target; 

public class SeaCamTargetGrouper : MonoBehaviour {
    static private SeaCamTargetGrouper _S;
    
    public enum eCamFocusMode { captains, manual };
    public eCamFocusMode focusMode = eCamFocusMode.captains; 

    public CinemachineTargetGroup cineTargetGroup;
    
    [SerializeField] List<CaptainRec> captsFocused;

    

    private void Awake() {
        _S = this;
    }

    void UpdateFocus() {
        switch ( focusMode ) {
        case eCamFocusMode.captains:
            List<Target> targetList = new List<Target>();
            Target targ;
            foreach ( CaptainRec cap in captsFocused ) {
                if ( cap.currentShip == null ) continue;
                targ = new Target();
                targ.target = cap.currentShip.transform;
                targ.weight = 1;
                targ.radius = 50;
                targetList.Add( targ );
            }
            cineTargetGroup.m_Targets = targetList.ToArray();
            break;
        }    
    }
    
    public static void UpdateCaptainsFocused( List<CaptainRec> tCapts = null ) {
        if ( tCapts != null ) {
            _S.captsFocused = tCapts;
        }
        _S.UpdateFocus();
    }
}
