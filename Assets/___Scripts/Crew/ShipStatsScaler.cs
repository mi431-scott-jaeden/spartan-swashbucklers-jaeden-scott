using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.Rendering;
using UnityEngine.Serialization;

/// <summary>
/// ShipStatsScaler will teach you about Near and Far, like Grover!
/// </summary>
public class ShipStatsScaler : MonoBehaviour {
    static public float secondsBetweenUpdates = 0.5f;
    static public float transitionHeight      = 1000;

    public Transform                 shipStatsTransform;
    public List<TransformThresholds> zoomLerpLevels;
    
    [NaughtyAttributes.ReadOnly]
    public int currentLevel;

    private ShipArrow[] shipArrows;

    public void SetColors(Color primary) {
        if (shipArrows == null) shipArrows = GetComponentsInChildren<ShipArrow>();
        foreach ( ShipArrow sA in shipArrows ) {
            sA.SetColors(primary);
        }
    }
    public void SetColors(Color primary, Color secondary) {
        if (shipArrows == null) shipArrows = GetComponentsInChildren<ShipArrow>();
        foreach ( ShipArrow sA in shipArrows ) {
            sA.SetColors(primary, secondary);
        }
    }

    // // Start has a return type of IEnumerator to make it run as a Coroutine
    // IEnumerator Start() {
    //     AttachStatsToTrans( 0 );
    //     while ( true ) {
    //         CheckNearVsFar();
    //         yield return new WaitForSecondsRealtime( secondsBetweenUpdates );
    //     }
    // }

    void CheckNearVsFar() {
        float zoomLerp = SeaCamera.ZOOM_LERP;
        int toLevel = GetLevelFromHeight( zoomLerp );
        if ( toLevel != currentLevel ) { AttachStatsToTrans( toLevel ); }
    }

    void AttachStatsToTrans( int toLevel = 0 ) {
        if ( zoomLerpLevels          == null
             || zoomLerpLevels.Count == 0
             || zoomLerpLevels.Count <= toLevel ) return;
        if ( shipStatsTransform != null ) {
            shipStatsTransform.SetParent( zoomLerpLevels[toLevel].transform, false );
            shipStatsTransform.localScale = Vector3.one;
        }
        currentLevel = toLevel;

        for ( int i = 0; i < zoomLerpLevels.Count; i++ ) {
            zoomLerpLevels[i].transform.gameObject.SetActive(i == toLevel);
        }
    }

    int GetLevelFromHeight( float zoomLerp ) {
        int toLevel = zoomLerpLevels.Count - 1;
        for ( int i = zoomLerpLevels.Count - 1; i >= 1; i-- ) {
            if ( zoomLerp <= zoomLerpLevels[i].zoomLerp ) { toLevel = i; } else { break; }
        }
        return toLevel;
    }


    [System.Serializable]
    public class TransformThresholds {
        [HideInInspector]
        public string name = "(Name will be set by zoomLerp)";
        [OnValueChanged( "UpdateName" )][AllowNesting]
        public float zoomLerp;
        public Transform transform;

        void UpdateName() {
            name = $"zoomLerp = {zoomLerp:0.##}";
        }
    }
}
