using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipArrow : MonoBehaviour {
    [SerializeField]
    private SpriteRenderer sRendPrimary, sRendSecondary;

    public void SetColors( Color primary ) {
        sRendPrimary.color = primary;
        sRendSecondary.enabled = false;
    }
    
    public void SetColors( Color primary, Color secondary  ) {
        sRendPrimary.color = primary;
        sRendSecondary.color = secondary;
        sRendSecondary.enabled = true;
    }
}
