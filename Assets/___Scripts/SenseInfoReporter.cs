using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-450)] // This should execute after GameManager and before SeaMap and Ship
public class SenseInfoReporter : MonoBehaviour {
    private Ship ship;
    private Port port;
    
    public bool       isShip { get; private set; }
    public bool       isPort { get; private set; }
    public Vector2Int mapLoc { get; private set; }
    
    void Awake() {
        ship = GetComponent<Ship>();
        isShip = ( ship != null );
        port = GetComponent<Port>();
        isPort = ( port != null );
    }

    void FixedUpdate() {
        // Gather relevant info
        if ( isShip ) {
            mapLoc = ship.mapLoc;
        } 
        // Report this info to the SeaMap
    }
    
    
}