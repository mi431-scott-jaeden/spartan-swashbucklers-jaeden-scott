using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using XnTools;

public class AStarPortsTest : MonoBehaviour {
    [Range( 1, 50 )]
    public int minPercentMapToTraverse = 25;
    public int  numPathsToShow = 10;
    public bool allowDiagonals = false;
    public bool yieldEachStep  = false;
    [ShowIf( "yieldEachStep" )]
    public float stepYieldDuration = 0.1f;
    
    private const int  diagonalCost   = 14;
    private const int  orthogonalCost = 10;

    public int        currentPath  = 0;
    public Vector2Int startMapLoc, endMapLoc;

    public List<AStarComparable>[] paths;


    private int                              minPathHCost;
    private XnPriorityQueue<AStarComparable> queue;
    private AStarComparable[,]               aStarGrid;

    void Start() {
        StartCoroutine( PathFindCoRo() );
    }

    IEnumerator PathFindCoRo() {
        paths = new List<AStarComparable>[numPathsToShow];
        queue = new XnPriorityQueue<AStarComparable>();
        while ( true ) {
            Init();
            // AStarComparable endNode = AStar();
            AStarComparable endNode = null;
            // Choose a currNode and endNode
            {
                AStarComparable currNode = new AStarComparable(startMapLoc);
                currNode.g = 0;
                currNode.h = GetHCost( currNode.mapLoc, endMapLoc );
                aStarGrid[currNode.mapLoc.x, currNode.mapLoc.y] = currNode;
                queue.Enqueue( currNode, currNode.f, currNode.h );
                while ( queue.Count > 0 ) {
                    currNode = queue.Dequeue();
                    currNode.inQueue = false;
                    if ( yieldEachStep ) {
                        paths[currentPath] = GeneratePath( currNode );
                        yield return new WaitForSeconds( stepYieldDuration ); //WaitForEndOfFrame();
                    }
                    if ( currNode.mapLoc == endMapLoc ) {
                        // We found the last node!!!
                        endNode = currNode;
                        break;
                    }
                    // Find and add neighbors
                    for ( int i = 0; i < orthogonalNeighbors.Length; i++ ) {
                        CheckNeighbor(currNode, orthogonalNeighbors[i], orthogonalCost);
                    }
                    if ( allowDiagonals ) {
                        for ( int i = 0; i < orthogonalNeighbors.Length; i++ ) {
                            CheckNeighbor(currNode, diagonalNeighbors[i], diagonalCost);
                        }
                    }
                }
            }
            paths[currentPath] = new List<AStarComparable>( GeneratePath( endNode ) );
            currentPath = ( currentPath + 1 ) % numPathsToShow;
            yield return new WaitForSeconds( 0.5f );
        }
    }

    void Init() {
        queue.Clear();
        aStarGrid = new AStarComparable[SeaMap.TEXTURE_RESOLUTION, SeaMap.TEXTURE_RESOLUTION];
        minPathHCost = SeaMap.TEXTURE_RESOLUTION * minPercentMapToTraverse / 100;
        // Choose a start and end point
        startMapLoc = SeaMap.MAP_PORTS[SeaMap.MAP_PORTS.RandomIndex()];
        int iterationCount = 0;
        do {
            endMapLoc = SeaMap.MAP_PORTS[SeaMap.MAP_PORTS.RandomIndex()];
            iterationCount++;
        } while ( (iterationCount < 100) && (GetHCost( startMapLoc, endMapLoc ) < minPathHCost) );
        // Now both start and end locs are chosen.
    }

    static private Vector2Int[] orthogonalNeighbors = new[] {
        new Vector2Int( 1, 0 ), new Vector2Int( 0, 1 ), new Vector2Int( -1, 0 ), new Vector2Int( 0, -1 )
    };
    static private Vector2Int[] diagonalNeighbors = new[] {
        new Vector2Int( 1, 1 ), new Vector2Int( -1, 1 ), new Vector2Int( -1, -1 ), new Vector2Int( 1, -1 )
    };

    // AStarComparable AStar() {
    //     AStarComparable currNode = new AStarComparable(startMapLoc);
    //     currNode.g = 0;
    //     currNode.h = GetHCost( currNode.mapLoc, endMapLoc );
    //     aStarGrid[currNode.mapLoc.x, currNode.mapLoc.y] = currNode;
    //     queue.Enqueue( currNode, currNode.priority );
    //     AStarComparable neighborNode;
    //     while ( queue.Count > 0 ) {
    //         currNode = queue.Dequeue();
    //         currNode.inQueue = false;
    //         if ( currNode.mapLoc == endMapLoc ) {
    //             // We found the last node!!!
    //             return currNode;
    //         }
    //         // Find and add neighbors
    //         for ( int i = 0; i < orthogonalNeighbors.Length; i++ ) {
    //             CheckNeighbor(currNode, orthogonalNeighbors[i], orthogonalCost);
    //         }
    //         if ( allowDiagonals ) {
    //             for ( int i = 0; i < orthogonalNeighbors.Length; i++ ) {
    //                 CheckNeighbor(currNode, diagonalNeighbors[i], diagonalCost);
    //             }
    //         }
    //     }
    //     // There was no path to the end. :(
    //     return null;
    // }

    void CheckNeighbor( AStarComparable currNode, Vector2Int offset, int cost ) {
        Vector2Int neighborLoc = currNode.mapLoc + offset;
        
        // Abort if the neighbor would be outside the grid.
        if ( neighborLoc.x < 0 || neighborLoc.x >= SeaMap.TEXTURE_RESOLUTION ) return;
        if ( neighborLoc.y < 0 || neighborLoc.y >= SeaMap.TEXTURE_RESOLUTION ) return;
        if ( !SeaMap.NAVIGABLE[neighborLoc.x, neighborLoc.y] ) return;
        
        AStarComparable neighborNode;
        if ( aStarGrid[neighborLoc.x, neighborLoc.y] == null ) {
            // Add a new node here
            neighborNode = new AStarComparable( neighborLoc );
            neighborNode.g = currNode.g + cost;
            neighborNode.h = GetHCost( neighborLoc, endMapLoc );
            aStarGrid[neighborLoc.x, neighborLoc.y] = neighborNode;
            neighborNode.prevNode = currNode;
            queue.Enqueue(neighborNode, neighborNode.f, neighborNode.h);
            neighborNode.inQueue = true;
        } else {
            neighborNode = aStarGrid[neighborLoc.x, neighborLoc.y];
            if ( neighborNode.inQueue ) {
                // Check the g cost
                if ( neighborNode.g > currNode.g + cost ) {
                    // Need to update the cost of neighborNode
                    int ndx = queue.IndexOfValue( neighborNode );
                    queue.RemoveAt( ndx );
                    neighborNode.g = currNode.g + cost;
                    neighborNode.prevNode = currNode;
                    queue.Enqueue( neighborNode, neighborNode.f, neighborNode.h );
                    neighborNode.inQueue = true;
                }
            }
        }
    }


    int GetHCost( AStarComparable a, AStarComparable b ) {
        return GetHCost( a.mapLoc, b.mapLoc );
    }
    int GetHCost( Vector2Int a, Vector2Int b ) {
        int hCost = 0;
        int x = Mathf.Abs( a.x - b.x );
        int y = Mathf.Abs( a.y - b.y );
        bool xGreater = ( x > y );
        if ( allowDiagonals ) {
            if ( xGreater ) {
                hCost = diagonalCost * y + orthogonalCost * ( x - y );
            } else {
                hCost = diagonalCost * x + orthogonalCost * ( y - x );
            }
        } else {
            hCost = (x + y) * orthogonalCost;
        }
        return hCost;
    }


    List<AStarComparable> GeneratePath( AStarComparable currNode ) {
        List<AStarComparable> aList = new List<AStarComparable>();
        do {
            if ( currNode.prevNode == null ) break;
            aList.Insert( 0, currNode );
            currNode = currNode.prevNode;
        } while ( currNode != null );
        return aList;
    }


    private void OnDrawGizmos() {
        if ( !Application.isPlaying || paths == null ) return;
        Vector3 a=Vector3.zero, b=Vector3.zero, endPos;
        for (int n=0; n<paths.Length; n++ ) {
            var nodeList = paths[n];
            if ( nodeList == null || nodeList.Count == 0 ) continue;
            // float u = ( n <= currentPath ) ? ( n + numPathsToShow ) - currentPath : n - currentPath;
            // u /= numPathsToShow;
            // Gizmos.color = Color.Lerp( Color.yellow, Color.red, u);
            Gizmos.color = Color.HSVToRGB( (float) n / ( paths.Length - 1 ), 1, 1 );
            for ( int i = 0; i < nodeList.Count-1; i++ ) {
                a = SeaMap.Position( nodeList[i].mapLoc );
                b = SeaMap.Position( nodeList[i + 1].mapLoc );
                Gizmos.DrawLine( a, b );
            }
            if ( yieldEachStep && n == currentPath ) {
                endPos = SeaMap.Position( endMapLoc );
                if ( b != endPos ) {
                    Gizmos.color = Color.white;
                    Gizmos.DrawLine( b, endPos );
                }
            }
        }
    }
}
