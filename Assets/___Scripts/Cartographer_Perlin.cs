using System.Collections;
using System.Collections.Generic;
using FastNoise;
using UnityEngine;
using NaughtyAttributes;
using UnityEditor.VisualStudioIntegration;
using XnTools;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class PerlinVars {                                                    // a
    public int numOctaves = 4;
    public float lacunarity = 2f;
    public float persistence = 0.5f;
}


[RequireComponent( typeof( MeshRenderer ) )]                                   // a
public class Cartographer_Perlin : MonoBehaviour {
    [Header( "Inscribed" )]                                                  // b
    public int        textureSize = 64;
    public PerlinVars perlinVars;
    public string     outputFileName = "SeaMap";
    [Range(0,1)]
    [OnValueChanged("GenerateTextureInEditorButton")]
    public float percentOcean = 0.7f;
    [Tooltip("The division between sea and land should be at 0.5f")]
    public Gradient seaLandGradient;
    [Range(0,0.8f)]
    [OnValueChanged("GenerateTextureInEditorButton")]
    public float seaLevelDeadZone = 0.2f;
    [Tooltip("The thickness of the border within which ports will not spawn.")]
    public int noPortsBorder = 8;
    
    [OnValueChanged("GenerateTextureInEditorButton")]
    public bool                    useFastNoiseLite = true;
    [OnValueChanged("GenerateTextureInEditorButton")]
    public FastNoiseLite.NoiseType fastNoisetype = FastNoiseLite.NoiseType.Perlin;
    public int numPorts = 20;
    // public AnimationCurve heightCurve;
    // public bool           

    [Header( "Inscribed / Dynamic" )]                                        // b
    [OnValueChanged("GenerateTextureInEditorButton")]
    public float noiseScale = 10;
    [OnValueChanged("GenerateTextureInEditorButton")]
    public                    Vector2   noiseOffset = Vector2.zero;
    
    private                   Texture2D tex;
    private List<Vector2Int>[] heightToCoords;
    
    

    Material              _mat;
    private Texture2D     cachedTexture2D;
    private FastNoiseLite fastNoise;

    void Start() {
        InitTex();
        UpdateTex();
    }

    void InitTex() {
        _mat = GetComponent<MeshRenderer>().sharedMaterial;
        // tex = Resources.Load<Texture2D>( textureFileName );
        // if ( tex == null ) {
        //     Debug.LogError($"Could not find the textureFileName \"{textureFileName}\" in a Resources folder!");
        //     return;
        // }

        heightToCoords = new List<Vector2Int>[256];

        for (int i = 0; i < 256; i++) {
            heightToCoords[i] = new List<Vector2Int>();
        }

        tex = new Texture2D( textureSize, textureSize, TextureFormat.RGBA32, false );                      // c
        tex.filterMode = FilterMode.Point;
        
        _mat.mainTexture = tex;

        if ( useFastNoiseLite ) {
            fastNoise = new FastNoiseLite();
            fastNoise.SetNoiseType( fastNoisetype );
            fastNoise.SetFractalType(FastNoiseLite.FractalType.FBm);
            fastNoise.SetFractalOctaves( perlinVars.numOctaves );
            fastNoise.SetFractalLacunarity( perlinVars.lacunarity );
            fastNoise.SetFractalGain( perlinVars.persistence );
        }
    }

    // void Update() {                                                          // d
    //     noiseOffset += offsetVel * Time.deltaTime;
    //     UpdateTex();
    // }

#if UNITY_EDITOR
    [Button( "Generate Texture in Editor" )]
    void GenerateTextureInEditorButton() {
        InitTex();
        UpdateTex();
    }

    [Button( "Write Map to Resources Folder" )]
    void WriteMapToFile() {
        byte[] bytes = tex.EncodeToPNG();
        ulong timeStamp = XnUtils.GenerateDateTimeUlong( XnUtils.eDateResolution.minutes );
        string textureFileName = $"{outputFileName}_{timeStamp}";
        System.IO.File.WriteAllBytes(Application.dataPath + $"/Resources/{outputFileName}.png", bytes);
        System.IO.File.WriteAllBytes(Application.dataPath + $"/Resources/{textureFileName}.png", bytes);
        Debug.Log( $"Wrote file \"<color=red>{textureFileName}.png\"</color> to Resources folder. To see it, refresh the Project pane." );
        AssetDatabase.Refresh();
        // Attempt to make the two files readable
        MakeTextureReadableAtPath(Application.dataPath + $"/Resources/{outputFileName}.png");
        MakeTextureReadableAtPath(Application.dataPath + $"/Resources/{textureFileName}.png");
    }

    void MakeTextureReadableAtPath(string path ) {
        //get the texture importer of the texture and set isReadable to true unless no texture is found at the path
        TextureImporter importer = (TextureImporter)AssetImporter.GetAtPath(path);
        if (importer != null)
            importer.isReadable = true;
        else
            Debug.Log($"No Importer found for the file at path: {path}");
    }

    [Button( "Assign Resources File to Texture" )]
    void AssignFileFromResourcesFolder() {
        tex = Resources.Load<Texture2D>( outputFileName );
        if ( tex == null ) {
            Debug.LogError($"Could not find the textureFileName \"{outputFileName}\" in a Resources folder!");
            return;
        }
        _mat.mainTexture = tex;
    }
#endif
    
    void UpdateTex() {                                                       // e
        if ( _mat == null || tex == null ) InitTex();

        // Generate height array
        float[] uVals = new float[textureSize * textureSize];
        int[] heights = new int[textureSize * textureSize];

        // Precalculate some math                                            // g
        float noiseMult = noiseScale / (float) textureSize;
        int ndx = 0;
        float x, y, u;
        int height;
        float minusHalf = -textureSize * 0.5f;
        float minVal = 0, maxVal = 0;
        for ( int v = 0; v < textureSize; v++ ) {
            for ( int h = 0; h < textureSize; h++, ndx++ ) {                 // h

                //u = Mathf.PerlinNoise( x, y );                               // j
                if ( useFastNoiseLite ) {
                    x = ( ( minusHalf + h ) * noiseMult * 100f ) + noiseOffset.x * 10f;       // i
                    y = ( ( minusHalf + v ) * noiseMult * 100f ) + noiseOffset.y * 10f;
                    u = fastNoise.GetNoise( x, y );
                } else {
                    x = ( ( minusHalf + h ) * noiseMult ) + noiseOffset.x;       // i
                    y = ( ( minusHalf + v ) * noiseMult ) + noiseOffset.y;
                    u = PerlinOctaves( perlinVars, x, y );
                }
                uVals[ndx] = u;
                if (u > maxVal) maxVal = u;
                else if (u < minVal) minVal = u;
            }
        }
        
        // Adjust the range to account for u values outside the range from -0.5...0.5
        float rangeMultiplier = 1f/(maxVal - minVal);
        ndx = 0;
        for ( int v = 0; v < textureSize; v++ ) {
            for ( int h = 0; h < textureSize; h++, ndx++ ) {                 // h
                u = (uVals[ndx] - minVal) * rangeMultiplier; // Mathf.InverseLerp(minVal, maxVal, uVals[ndx]);
                height = Mathf.Clamp( Mathf.RoundToInt(u * 255), 0, 255);
                heights[ndx] = height;
                heightToCoords[height].Add(new Vector2Int(h,v));
            }
        }
        
        // Determine where the cutoff should be
        int totalNumberOfPixels = textureSize * textureSize;
        int numberOfOceanPixels = (int) (percentOcean * totalNumberOfPixels);
        int accumulatedPixels = 0;
        int seaLevelH;

        for (seaLevelH = 0; seaLevelH < 256; seaLevelH++) {
            accumulatedPixels += heightToCoords[seaLevelH].Count;

            if (accumulatedPixels >= numberOfOceanPixels) {
                break;
            }
        }
        
        // Generate the array to fill
        Color[] pixels = new Color[textureSize * textureSize];               // f
        float remapToOcean = 1f / seaLevelH;
        float remapToLand = 1f / (255 - seaLevelH);
        Color col;
        
        ndx = 0;
        float seaLevelHf = seaLevelH;
        float heightsAboveSeaLevel = 255 - seaLevelH;
        float seaCeiling = (1f - seaLevelDeadZone) * 0.5f;
        float landFloor = (1f + seaLevelDeadZone) * 0.5f;
        for (int v = 0; v < textureSize; v++) {
            for (int h = 0; h < textureSize; h++, ndx++) {
                height = heights[ndx];

                if (height > 250) {
                    Debug.Log("Break");
                }

                if (height <= seaLevelH) {
                    u = height / seaLevelHf * seaCeiling;
                    // u = remapToOcean * height;
                } else {
                    u = landFloor + ((height - seaLevelHf) / heightsAboveSeaLevel * seaCeiling);
                    // u = 0.5f + (remapToLand * (height - seaLevelH));
                }
                u = Mathf.Clamp01( u );
                col = seaLandGradient.Evaluate(u);
                pixels[ndx] = col;
            }
        }
        
        // Set port pixels.
        int portsPlaced = 0;
        // int[] heightIndices = { 127, 126, 128, 125, 129, 124, 130, 123, 131, 122, 132, 121, 133, 120, 134 };
        // int[] heightIndices = { 128, 129, 130, 131, 132, 133, 134 };
        // int[] heightIndices = { 0, -1, 1, -2, 2, -3, 3, -4, 4, -5, 5, -6, 6, -7, 7 };
        int[] heightIndices = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
        for ( int i = 0; i < heightIndices.Length; i++ ) {
            heightIndices[i] = heightIndices[i] + seaLevelH;
        }
        int heightIndexNum = 0;
        int index;
        Color portColor = new Color( 1, 0, 0, 0.5f );
        List<Vector2Int> portLocsAtHeight = new List<Vector2Int>(); 
        Vector2Int loc;
        for (heightIndexNum=0; heightIndexNum<heightIndices.Length; heightIndexNum++) {
            portLocsAtHeight.Clear();
            portLocsAtHeight.AddRange(heightToCoords[heightIndices[heightIndexNum]]);
            while ( portLocsAtHeight.Count > 0 ) {
                int n = Random.Range( 0, portLocsAtHeight.Count );
                loc = portLocsAtHeight[n];
                index = Vec2Index( loc );
                portLocsAtHeight.RemoveAt( n );
                bool withinBorder = WithinBorder( loc );
                bool onTheCoast = OnTheCoast( loc, ref pixels );
                bool awayFromPorts = AwayFromOtherPorts( loc, ref pixels, 2 );
                if ( withinBorder && onTheCoast && awayFromPorts) {
                    pixels[index] = portColor;
                    portsPlaced++;
                    Debug.Log($"Port {portsPlaced:00} at: {loc.x:000}, {loc.y:000}"  );
                    if ( portsPlaced >= numPorts ) break;
                }
            }
            // foreach ( Vector2Int loc in portLocsAtHeight ) {
            //     index = Vec2Index( loc );
            //     pixels[index] = portColor;
            //     portsPlaced++;
            //     if ( portsPlaced >= numPorts ) break;
            // }
            if ( portsPlaced >= numPorts ) break;
            heightIndexNum++;
        }

        tex.SetPixels( pixels );                                            // l
        tex.Apply( false );


        // tex = Resources.Load<Texture2D>( textureFileName );
        // if ( tex == null ) {
        //     Debug.LogError($"Could not find the textureFileName \"{textureFileName}\" in a Resources folder!");
        //     return;
        // }
        // _mat.mainTexture = tex;
    }

    bool WithinBorder(Vector2Int loc) {
        if ( loc.x > noPortsBorder && loc.x < textureSize - noPortsBorder ) {
            if ( loc.y > noPortsBorder && loc.y < textureSize - noPortsBorder ) {
                return true;
            }
        }
        return false;
    }

    bool OnTheCoast( Vector2Int loc, ref Color[] pixels ) {
        List<Vector2Int> offsets = PixelRadius.GetPixelRadius( 1 );
        Vector2Int v2i;
        int ndx;
        bool nearWater = false, nearLand = false;
        foreach ( Vector2Int offset in offsets ) {
            v2i = loc + offset;
            ndx = Vec2Index( v2i );
            if ( ndx < 0 || ndx >= pixels.Length ) continue;
            if ( pixels[ndx].a < 0.5f ) nearWater = true;
            if ( pixels[ndx].a > 0.5f ) nearLand = true;
            if ( nearWater && nearLand ) return true;
        }
        return false;
    }
    
    bool AwayFromOtherPorts( Vector2Int loc, ref Color[] pixels, int radius = 1 ) {
        for ( int i = 0; i <= radius; i++ ) {
            List<Vector2Int> offsets = PixelRadius.GetPixelRadius( i );
            Vector2Int v2i;
            int ndx;
            foreach ( Vector2Int offset in offsets ) {
                v2i = loc + offset;
                ndx = Vec2Index( v2i );
                if ( ndx < 0 || ndx >= pixels.Length ) continue;
                if ( Mathf.Approximately( pixels[ndx].a, 0.5f ) ) return false;
            }
        }
        return true;
    }

    int Vec2Index( Vector2Int v2i ) {
        return v2i.y * textureSize + v2i.x;
    }
    
    public float PerlinOctaves( PerlinVars pVars, float x, float y ) {
        float u = 0, lacu = 1, pers = 1, noiseAmt;
        for ( int octave = 0; octave < pVars.numOctaves; octave++ ) {
            // l = Mathf.Pow( lacu, o );                                     // e
            // p = Mathf.Pow( pers, o );
            if ( octave != 0 ) {                                             // f
                lacu *= pVars.lacunarity;
                pers *= pVars.persistence;
            }
            noiseAmt = ( Mathf.PerlinNoise( x * lacu, y * lacu ) - 0.5f ) * pers;
            u += noiseAmt;
        }
        return u; // Will generally range from [-1..1], but it will almost certainly be beyond that due to persistence of higher octaves.
    }
}
