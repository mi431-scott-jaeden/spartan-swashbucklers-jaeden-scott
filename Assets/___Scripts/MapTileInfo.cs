using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapTileInfo {
    public enum eType { sea, land, port };
    public bool      navigable   = false;
    public eMapSense mapTileType = 0;

    public  Vector2Int      mapLoc;
    private eType           _type;
    private List<Ship>      _ships;
    
    private SenseInfo[]       portInfos;
    private List<SenseInfo>[] senseInfoLists;
    private bool[]            senseInfosGathered;

    public MapTileInfo(Vector2Int tMapLoc) {
        mapLoc = tMapLoc;
        senseInfoLists = new List<SenseInfo>[SpartanSwashbucklersSettings_SO.MAP_SENSE_MAX_DISTANCE_PLUS_1];
        senseInfosGathered = new bool[5];
        _ships = new List<Ship>();
        ClearSenseInfos();
        SeaMap.CLEAR_SENSE_INFOS += ClearSenseInfos;
    }

    void ClearSenseInfos() {
        for ( int i = 0; i < SpartanSwashbucklersSettings_SO.MAP_SENSE_MAX_DISTANCE_PLUS_1; i++ ) {
            senseInfosGathered[i] = false;
        }
        foreach ( List<SenseInfo> list in senseInfoLists)
        {
            if (list != null) list.Clear();
        }
        allSenseInfosAround = null;
    }

    public List<SenseInfo> GatherSenseInfosAtDistance( int distance ) {
        if ( distance > SpartanSwashbucklersSettings_SO.MAP_SENSE_MAX_DISTANCE_PLUS_1 ) return null;
        if ( senseInfosGathered[distance] ) {
            return senseInfoLists[distance];
        }
        // Looks like we need to gather this one
        if ( senseInfoLists[distance] == null ) senseInfoLists[distance] = new List<SenseInfo>();
        // Port information
        if ( portInfos != null ) {
            if ( distance < portInfos.Length ) {
                if ( portInfos[distance].valid ) {
                    senseInfoLists[distance].Add( portInfos[distance] );
                }
            }
        }
        // Ships information
        foreach ( Ship ship in _ships ) {
            senseInfoLists[distance].Add( ship.GetSenseInfoAtDistance( distance ) );
        }
        senseInfosGathered[distance] = true;
        return senseInfoLists[distance];
    }

    public List<SenseInfo> allSenseInfosAround;

    /// <summary>
    /// This is called by SeaMap when it adds a port to this MapTileInfo.
    /// </summary>
    /// <param name="portType"></param>
    public void AddPortInfo(eMapSense portType) {
        portInfos = new SenseInfo[SpartanSwashbucklersSettings_SO.MAP_SENSE_MAX_DISTANCE_PLUS_1];
        for ( int i = 0; i < SpartanSwashbucklersSettings_SO.MAP_SENSE_MAX_DISTANCE_PLUS_1; i++ ) {
            portInfos[i] = new SenseInfo( i, portType, mapLoc );
            // eMapSense senseAtDist = eMapSense.none;
            // if ( i <= GameManager.SETTINGS.mapSenseDistDict[eMapSense.port] ) {
            //     senseAtDist |= eMapSense.port;
            // }
            // if ( i <= GameManager.SETTINGS.mapSenseDistDict[portType] ) {
            //     senseAtDist |= portType;
            // }
            // if ( senseAtDist != eMapSense.none ) {
            //     portInfos[i] = new SenseInfo {
            //         distance = i,
            //         senseType = senseAtDist, 
            //         mapLoc = this.mapLoc
            //     };
            // }
        }
    }

    public void RemoveShip( Ship ship ) {
        _ships.Remove( ship );
    }

    public void AddShip( Ship ship ) {
        if (!_ships.Contains(ship)) _ships.Add( ship );
    }
}

// /// <summary>
// /// Information about any ship. If the value is -1 (or [-1,-1,-1]), it means that the value is not known.
// /// </summary>
// public struct ShipInfo {
//     public Vector2Int mapLoc;
//     public Vector3    position;
//     public int        health,  wealth, crew;
//     public float      heading, speed,  speedMax;
//     public int        sailsUp;
//     public int        numCannon, cannonLoaded;
//     public int        bankedWealth;
//
//     // public ShipInfo( Ship subject, Ship observer ) {
//     //     Vector3 delta = subject.transform.position - observer.transform.position;
//     //     delta.y = 0;
//     //     float dist = delta.magnitude;
//     //     
//     // }    
// }

