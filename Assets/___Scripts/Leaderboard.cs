using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Leaderboard : MonoBehaviour {
    public const char   selectedChar = '«';
    public const string blockString  = "▁▂▃▄▅▆▇██";
    public const char   pirateChar   = '☠';
    
    
    public enum eSortType { Money, Kills, Deaths, Name }
    [SerializeField] List<CaptainRec> capts;
    List<CaptainRec> captsFocused;
    TMP_Text                          tmpText;
    eSortType                         sortType;

    public int   firstIndent = 200,       tabIndent = 128;
    public Gradient faceColors;

    public Image buttonImage;

    // Start is called before the first frame update
    void Awake()
    {
        // Create a sorted version of the list of CaptainRec
        capts = new List<CaptainRec>(GameManager.SETTINGS.captainRecs);
        captsFocused = new List<CaptainRec>();
        tmpText = GetComponentInChildren<TMP_Text>();
    }

    void FixedUpdate()
    {
        /*if (capts == null)
        {
            if (GameManager.SETTINGS == null ||
                GameManager.SETTINGS.captainRecs == null)
            {
                return;
            }

            // Create a sorted version of the list of CaptainRec
            capts = new List<CaptainRec>(GameManager.SETTINGS.captainRecs);
        }*/

        // Sort all the captains by values and name
        // This shows calling the Sort(Comparison<T>) overload using 
        //   an anonymous method for the Comparison delegate. 
        // This method sorts by value with the greater value first.
        // If value are the same, it sorts alphabetically by name
        capts.Sort(delegate (CaptainRec a, CaptainRec b)
        {
            switch (sortType)
            {
                case eSortType.Money:
                    if (a.moneyTotal < b.moneyTotal) return 1;
                    if (a.moneyTotal > b.moneyTotal) return -1;
                    break;
                case eSortType.Kills:
                    if (a.killsTotal < b.killsTotal) return 1;
                    if (a.killsTotal > b.killsTotal) return -1;
                    break;
                case eSortType.Deaths:
                    if (a.deathsTotal < b.deathsTotal) return -1;
                    if (a.deathsTotal > b.deathsTotal) return 1;
                    break;
                case eSortType.Name:
                    return a.name.CompareTo(b.name);
            }
            return a.name.CompareTo(b.name);
        });

        // Find Max/Min for moneyTotal, kills, and deaths
        Vector2 v2Money = Vector2.one, v2Kills = Vector2.one, v2Deaths = Vector2.one;
        bool gradientValsInited = false;
        for (int i = 0; i < capts.Count; i++)
        {
            if (!capts[i].spawnInGame) continue;
            
            if (!gradientValsInited)
            {
                v2Money.x = v2Money.y = capts[i].moneyTotal;
                v2Kills.x = v2Kills.y = capts[i].killsTotal;
                v2Deaths.x = v2Deaths.y = capts[i].deathsTotal;
                gradientValsInited = true;
            }
            else
            {
                v2Money.x = Mathf.Min(v2Money.x, capts[i].moneyTotal);
                v2Money.y = Mathf.Max(v2Money.y, capts[i].moneyTotal);

                v2Kills.x = Mathf.Min(v2Kills.x, capts[i].killsTotal);
                v2Kills.y = Mathf.Max(v2Kills.y, capts[i].killsTotal);

                v2Deaths.x = Mathf.Min(v2Deaths.x, capts[i].deathsTotal);
                v2Deaths.y = Mathf.Max(v2Deaths.y, capts[i].deathsTotal);
            }
        }

        int tabPos = firstIndent;
        System.Text.StringBuilder sbN = new System.Text.StringBuilder();
        string[] headers = { "Name", "Kil", "Dth", "$" };
        sbN.Append( headers[0] );
        for ( int i = 1; i < headers.Length; i++ ) {
            sbN.Append( "<pos=" + tabPos + ">"+headers[i] );
            tabPos += tabIndent;
        }
        sbN.Append( "\n" );
        
        // sbN.AppendLine("Name"); // "Name\tMon\tKil\tDth\n"
        // sbN.AppendLine("Mon\tKil\tDth");
        float u;
        Color gray;
        foreach (CaptainRec cap in capts)
        {
            tabPos = 0;

            if ( !cap.spawnInGame ) continue;

            sbN.Append( HealthCharFromCaptainRec(cap) );
            sbN.Append("<color=#");
            sbN.Append(ColorUtility.ToHtmlStringRGB(cap.primaryColor)); //sbN.Append(HexConverter((com.activeAgent == null) ? Color.black : com.color));
            sbN.Append('>');
            sbN.Append(cap.initials.Substring(0, 2));
            // sb.Append("</color>");
            sbN.Append("</color><color=#");
            sbN.Append(ColorUtility.ToHtmlStringRGB(cap.secondaryColor)); //sbN.Append(HexConverter((com.activeAgent == null) ? Color.black : com.color2));
            // sb.Append(HexConverter(com.color2));
            sbN.Append('>');
            sbN.Append(cap.initials.Substring(2));
            // This didn't work because it was too wide and the skull wasn't in the font I used. - JGB 2020-08-13
            // sbN.Append("</color>");
            sbN.Append("</color> ");
            if ( captsFocused.Contains( cap ) ) {
                sbN.Append( selectedChar );
            }

            tabPos += firstIndent;
            sbN.Append("<pos=" + tabPos + ">");
            sbN.Append(cap.killsTotal + " (" + cap.killsPVP + ")");
            tabPos += tabIndent;
            sbN.Append("<pos=" + tabPos + ">");
            sbN.Append(cap.deathsTotal + " (" + cap.deathsPVP + ")");
            tabPos += tabIndent;
            sbN.Append("<pos=" + tabPos + ">");
            sbN.Append(cap.moneyTotal + " (" + cap.moneyOnShip + ")");
            sbN.Append('\n');
        }

        tmpText.text = sbN.ToString();
    }

    public void ChangeSortType(int sortType)
    {
        this.sortType = (eSortType)sortType;
    }

    public void Click() {
        Vector3 mPos = Input.mousePosition;
        mPos = TransformTo1080p( mPos );
        //mPos.x = mPos.x * 1920f / Screen.width;
        //mPos.y = mPos.y * 1080f / Screen.height;

        Vector3 txtPos = TransformTo1080p( tmpText.rectTransform.position );
        mPos -= txtPos;

        Debug.Log( "Clicked @ "+mPos );

        TMP_TextInfo textInfo = tmpText.textInfo;
        for (int i=0; i<textInfo.lineInfo.Length; i++ ) {
            if ( textInfo.lineInfo[i].lineExtents.min.y < mPos.y
                && textInfo.lineInfo[i].lineExtents.max.y > mPos.y ) {
                string txt = tmpText.GetParsedText().Substring( textInfo.lineInfo[i].firstCharacterIndex, textInfo.lineInfo[i].characterCount );
                //Debug.Log( "Clicked line " + i + "\t" + txt );
                string name = txt.Split( ' ' )[0][1..];
                //Debug.Log( "Clicked Name: " + name );
                foreach (CaptainRec cap in capts){
                    if (name == cap.initials) {
                        if ( Input.GetKey( KeyCode.LeftShift ) ) {
                            ToggleFocus( cap );
                        } else {
                            SoloFocus( cap );
                        }
                        // //Debug.Log( "Found Captain: " + cap.name );
                        // SeaCamera.FOCUS_SHIP( cap );
                    }
                }
            }
        }
    }

    void ToggleFocus( CaptainRec cap ) {
        if ( captsFocused.Contains( cap ) ) {
            captsFocused.Remove( cap );
        } else {
            captsFocused.Add( cap );
        }
        SeaCamTargetGrouper.UpdateCaptainsFocused( captsFocused );
    }
    void SoloFocus( CaptainRec cap ) {
        captsFocused.Clear();
        captsFocused.Add( cap );
        SeaCamTargetGrouper.UpdateCaptainsFocused( captsFocused );
    }

    Vector3 TransformTo1080p(Vector3 v) {
        v.x = v.x * 1920f / Screen.width;
        v.y = v.y * 1080f / Screen.height;
        return v;
    }

    char HealthCharFromCaptainRec( CaptainRec cap ) {
        if ( cap.currentShip == null ) return pirateChar;
        float healthPercent = cap.currentShip.healthPercent;
        healthPercent *= blockString.Length;
        int healthInt = Mathf.FloorToInt( healthPercent );
        healthInt = Mathf.Clamp( healthInt, 0, blockString.Length - 1 );
        return blockString[healthInt];
    }
    
    
    // string HealthToBlockString(Agent a, bool withColor=true) {
    //     float u = a.health / (float) ArenaManager.AGENT_SETTINGS.agentHealthMax;
    //     string block = blockString[Mathf.FloorToInt( u * 8 )].ToString();
    //     if (withColor) {
    //         Color c = faceColors.Evaluate( u );
    //         return "<color=" + HexConverter( c ) + ">" + block + "</color>";
    //     }
    //     return block;
    // }
    //
    // string AmmoToBlockString( Agent a, bool withColor = true ) {
    //     float u = a.ammo / (float) ArenaManager.AGENT_SETTINGS.agentAmmoMax;
    //     string block = blockString[Mathf.FloorToInt( u * 8 )].ToString();
    //     if ( withColor ) {
    //         Color c = Color.Lerp( Color.black, Color.white, u );//faceColors.Evaluate( u );
    //         return "<color=" + HexConverter( c ) + ">" + block + "</color>";
    //     }
    //     return block;
    // }
}










