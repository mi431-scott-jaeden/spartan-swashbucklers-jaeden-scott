using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaFollowsCamera : MonoBehaviour {
    [Header("Inscribed")] 
    [SerializeField] SeaCamera seaCamera;
    [SerializeField] bool snapYToGrid = false;
    [SerializeField] float planeSize = 10; // This is because the Unity default Plane mesh is 10x10m

    [Header("Dynamic")] 
    [SerializeField] Vector3 gridSize;

    void Start() {
        MeshRenderer waterRend = GetComponentInChildren<MeshRenderer>();

        if (waterRend != null) {
            gridSize = waterRend.transform.lossyScale * planeSize;
        }
    }

    void FixedUpdate() {
        Vector3 gridLoc = ToGrid(transform.position);
        Vector3 seaCamGridLoc = ToGrid(seaCamera.transform.position);

        if (gridLoc != seaCamGridLoc) {
            transform.position = seaCamGridLoc;
        }
    }

    Vector3 ToGrid(Vector3 vIn) {
        Vector3 vOut = vIn;
        vOut.x = Mathf.Round(vIn.x / gridSize.x) * gridSize.x;
        if (snapYToGrid) vOut.y = Mathf.Round(vIn.y / gridSize.y) * gridSize.y;
        vOut.z = Mathf.Round(vIn.z / gridSize.z) * gridSize.z;
        return vOut;
    }
}
