using System;
using System.Collections.Generic;

namespace XnTools {
    // Based on: https://stackoverflow.com/questions/1937690/c-sharp-priority-queue
    public class XnPriorityQueue<T> {
        SortedList<Pair<int>, T> _sList;
        int                      tiebreakerCount;

        public XnPriorityQueue() {
            _sList = new SortedList<Pair<int>, T>( new PairComparer<int>() );
        }

        public void Enqueue( T item, int priority, int priority2 = 0 ) {
            priority2 = ( priority2 * 10000 ) + tiebreakerCount;
            tiebreakerCount++;
            if ( tiebreakerCount >= 10000 )
                tiebreakerCount = 0; // Stop overflow, though it should be very rare. - JGB 2023-03-27

            _sList.Add( new Pair<int>( priority, priority2 ), item );
        }

        public T Dequeue() {
            T item = _sList[_sList.Keys[0]];
            _sList.RemoveAt( 0 );
            return item;
        }

        public int Count {
            get { return _sList.Count; }
        }

        public int IndexOfValue( T value ) {
            return _sList.IndexOfValue( value );
        }

        public void RemoveAt( int index ) {
            _sList.RemoveAt( index );
        }

        public void Remove( T value ) {
            _sList.RemoveAt( IndexOfValue( value ) );
        }

        public void Clear() {
            _sList.Clear();
        }
    }

    public class Pair<T> {
        public T First  { get; private set; }
        public T Second { get; private set; }

        public Pair( T first, T second ) {
            First = first;
            Second = second;
        }

        public override int GetHashCode() {
            return First.GetHashCode() ^ Second.GetHashCode();
        }

        public override bool Equals( object other ) {
            Pair<T> pair = other as Pair<T>;
            if ( pair == null ) { return false; }
            return ( this.First.Equals( pair.First ) && this.Second.Equals( pair.Second ) );
        }
    }

    public class PairComparer<T> : IComparer<Pair<T>> where T : System.IComparable {
        public int Compare( Pair<T> x, Pair<T> y ) {
            if ( x.First.CompareTo( y.First ) < 0 ) {
                return -1;
            } else if ( x.First.CompareTo( y.First ) > 0 ) {
                return 1;
            } else { return x.Second.CompareTo( y.Second ); }
        }
    }
}