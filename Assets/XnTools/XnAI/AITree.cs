using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using NaughtyAttributes;
using UnityEditor.Animations;
using Type = System.Type;
using MethodInfo = System.Reflection.MethodInfo;
using Exception = System.Exception;
#if UNITY_EDITOR
    using UnityEditor;
#endif

namespace XnTools.XnAI {
    // public class XnAI : MonoBehaviour {
    //     public AITree aiTree;
    //
    //     protected virtual void Reset() {
    //         aiTree = new AITree( this );
    //     }
    //
    //     protected virtual void OnValidate() {
    //         aiTree.SetAITreeAndParent();
    //     }
    // }
    //
    // public abstract class XnAI_SO : ScriptableObject {
    //     public AITree aiTree;
    //
    //     protected virtual void Reset() {
    //         aiTree = new AITree( this );
    //     }
    //
    //     protected virtual void OnValidate() {
    //         aiTree.SetAITreeAndParent();
    //     }
    // }


    // public abstract class AINode { }

    [System.Serializable]
    public class AITree {

        [HideInInspector]
        public MonoBehaviour aiScript;
        [HideInInspector]
        public iXnAI_SO aiSO;
        [HideInInspector]
        public Type aiScriptType;
        [HideInInspector]
        public bool isScriptableObject = false;

        private bool isPlaying => ( Application.isPlaying );
        
        // [ResizableTextArea][ShowIf("isPlaying")][AllowNesting]
        // public string treeStatusWhilePlaying = "";
        [SerializeField, SerializeReference][ShowIf("isPlaying")][AllowNesting]
        private InfoProperty treeStatusInfo = new InfoProperty("Behavior Tree Status", "", false);
        private List<string> treeStatusList = new List<string>();
        private string       _treeStatusString;
        private bool         _treeStatusStringDirty = true;
        public string treeStatusString {
            get {
                if ( _treeStatusStringDirty ) {
                    if ( treeStatusList == null ) {
                        treeStatusString = "N/A";
                    } else {
                        treeStatusString = String.Join('\t', treeStatusList);
                    }
                }
                return _treeStatusString;
            }
            set {
                _treeStatusString = value;
                _treeStatusStringDirty = false;
                if ( isScriptableObject ) aiSO?.UpdateAITreeStatus( _treeStatusString );
            }
        }
        
#if UNITY_EDITOR
        protected bool isNotPaused => Application.isPlaying && !UnityEditor.EditorApplication.isPaused;
#else
    protected bool isNotPaused => Application.isPlaying;
#endif
        
        [SerializeField, SerializeReference]
        [HideIf("isNotPaused")]
        [Label( "GetChildName" )][AllowNesting]
        public iAINode child;
        protected string GetChildName() => ( this.child == null ) ? "Click \"Reset AITree\" below." : $"{this.child.nodeName}";

        [HideInInspector]
        public List<string> utilityMethods, utilityDecoratorMethods, executeMethods;
        

        // public AITree( MonoBehaviour tAIScript ) {
        //     aiScript = tAIScript;
        //     aiScriptType = tAIScript.GetType();
        //     child = new AINode_Composite();
        //     child?.SetAITreeAndParent( this, null );
        // }
        
        public AITree( iXnAI_SO tAIScriptSO ) {
            SetXnAI_SO( tAIScriptSO );
            AddRootCompositeNode();
        }

        public void SetXnAI_SO( iXnAI_SO tAIScriptSO ) {
            aiSO = tAIScriptSO;
            isScriptableObject = true;
            aiScriptType = tAIScriptSO.GetType();
            SetAITreeAndParent();
        }

        public void SetAITreeAndParent() {
            child?.SetAITreeAndParent( this, null );
        }


        private int _nodeNum = 0;
        public  int nextNodeNum => ++_nodeNum;


#region Runtime Methods

        public eStatus Execute() {
            if ( treeStatusList == null ) treeStatusList = new List<string>();
            treeStatusList.Clear();
            _treeStatusStringDirty = true;
            eStatus tStatus = eStatus.failed;
            if ( child == null ) {
                treeStatusList.Add($"{aiSO} you need to Reset the AITree!"  );
            } else {
                tStatus = child.Execute( ref treeStatusList, 1 );
            }
            treeStatusString = String.Join('\n', treeStatusList);
            
            // if ( treeStatusInfo == null ) {
            //     treeStatusInfo = new InfoProperty("Behavior Tree Status", "", false);
            // }
            // treeStatusInfo.title = "Behavior Tree Status";
            // treeStatusInfo.text = String.Join('\n', treeStatusList);
            
            // treeStatusWhilePlaying = String.Join('\n', treeStatusList);
            
            return tStatus;
        }
        
#endregion
        
        
        public void AddRootCompositeNode() {
            child = new AINode_Composite();
            child.nodeName = "Root Composite Node";
            child.SetAITreeAndParent( this, null );
        }
        

        public bool SelectNode( iAINode node ) {
            if ( isScriptableObject ) {
                if ( aiSO != null ) {
                    iAINode prevNode = aiSO.selectedNode;
                    aiSO.selectedNode = node;
                    if ( prevNode != null ) prevNode.UpdateThisNodeSelected();
                    return true;
                }
            }
            return false;
        }

        public iAINode GetSelectedNode() {
            if ( !isScriptableObject || aiSO == null ) return null;
            return aiSO.selectedNode;
        }

        public bool DeselectNode( iAINode node ) {
            if ( isScriptableObject ) {
                if ( aiSO != null ) {
                    if ( aiSO.selectedNode == node ) {
                        aiSO.selectedNode = null;
                        return true;
                    }
                }
            }
            return false;
        }
        
        
        /// <summary>
        /// Given the Type of the parent object (which has this AITree as its child),
        ///  collect the parent's methods that can return utility or status information.
        /// </summary>
        /// <param name="parentType">The System.Type of the parent</param>
        public void GenerateAITreeMethodNames(Type parentType) {
            utilityMethods = parentType.GetMethodsByReturnTypeAndParameters( typeof(aFloat) );
            utilityMethods.Sort();
            
            executeMethods = parentType.GetMethodsByReturnTypeAndParameters( typeof(eStatus) );
            executeMethods.Sort();
            executeMethods.Insert( 0, iAINode.nullMethodName );

            utilityDecoratorMethods = parentType.GetMethodsByReturnTypeAndParameters( typeof(aFloat), typeof(aFloat) );
            utilityDecoratorMethods.Sort();
            utilityDecoratorMethods.Insert(0, $"{iAINode.nullMethodChar}--- Utility Decorator Methods ---");
            utilityDecoratorMethods.Add($"{iAINode.nullMethodChar}--- Utility Methods (no aFloat parameter) ---");
            utilityDecoratorMethods.AddRange( utilityMethods );
            
            // Finally, add " null" to the beginning of the utilityMethods
            utilityMethods.Insert( 0, iAINode.nullMethodName );
        }
        
        /// <summary>
        /// Given the parent object (which has this AITree as its child),
        ///  collect the parent's methods that can return utility or status information.
        /// </summary>
        /// <param name="parentObject">The parent itself</param>
        public void GenerateAITreeMethodNames( object parentObject ) {
            GenerateAITreeMethodNames(parentObject.GetType());
        }


#region MethodInfo caching using MethodRec and methodRecDict - JGB 2023-04-25
        public class MethodRec {
            public string     name;
            public MethodInfo methodInfo;
            public int        numParameters = 0;
            public Type[]     parameterTypes = Type.EmptyTypes;

            public MethodRec( string n, MethodInfo mi ) {
                name = n;
                methodInfo = mi;
                if ( mi != null ) {
                    System.Reflection.ParameterInfo[] paramInfos = mi.GetParameters();
                    if ( paramInfos.Length > 0 ) {
                        numParameters = paramInfos.Length;
                        parameterTypes = new Type[paramInfos.Length];
                        for ( int i = 0; i < paramInfos.Length; i++ ) {
                            parameterTypes[i] = paramInfos[i].ParameterType;
                        }
                    }
                }
            }
        }


        private Dictionary<string, MethodRec> _methodRecDict;
        Dictionary<string, MethodRec> methodRecDict {
            get {
                if ( _methodRecDict == null ) {
                    _methodRecDict = new Dictionary<string, MethodRec>();
                }
                return _methodRecDict;
            }
        }
        
        /// <summary>
        /// Finds a MethodInfo by methodName. The MethodInfo is cached in methodRecDict to improve performance.<para />
        /// Note: If no valid method exists, null be returned (and is also cached in methodRecDict).
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="parameterTypes"></param>
        /// <returns></returns>
        MethodInfo GetMethodInfo(string methodName, Type[] parameterTypes = null) {
            if (parameterTypes == null) parameterTypes = Type.EmptyTypes;
            if (methodRecDict.ContainsKey(methodName)) 
                return methodRecDict[methodName].methodInfo;
            // If it's not in the methodRecDict, use GetMethodRec() to add it.
            return GetMethodRec(methodName, parameterTypes)?.methodInfo;
            // The ?. above will return null if there is no method methodName
        }
        
        /// <summary>
        /// Finds a MethodRec by methodName. The MethodInfo is cached in methodRecDict to improve performance.<para />
        /// Note: If no valid method exists, null be returned (and is also cached in methodRecDict).
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="parameterTypes"></param>
        /// <returns></returns>
        MethodRec GetMethodRec(string methodName, Type[] parameterTypes = null) {
            if (parameterTypes == null) parameterTypes = Type.EmptyTypes;
            if (methodRecDict.ContainsKey(methodName)) 
                return methodRecDict[methodName];
            // First search with any parameters passed in
            MethodInfo mInfo = UnityEventBase.GetValidMethodInfo( aiSO, methodName, parameterTypes );
            // Check for version with no parameters just in case (this is helpful for non-decorator Utility methods 
            if (mInfo == null && parameterTypes != Type.EmptyTypes) 
                mInfo = UnityEventBase.GetValidMethodInfo( aiSO, methodName, Type.EmptyTypes );
            if ( mInfo == null ) {
                methodRecDict.Add(methodName, null);
                return null;
            } else {
                MethodRec mRec = new MethodRec(methodName, mInfo);
                methodRecDict.Add(methodName, mRec);
                return mRec;
            }
        }
#endregion
        
#region Invoke methods by name - JGB 2023-04-25

        public iAINode currentNode;
        public void AddStatusNote( string note ) {
            if ( currentNode != null ) {
                // currentNode.statusNote += " | " + note;
                currentNode.statusNote = note;
            }
            
        }
            
        public aFloat InvokeCheckUtility(string utilityMethod) {
            if ( utilityMethod == iAINode.nullMethodName ) return iAINode.defaultUtility;

            try {
                MethodInfo methodInfo = GetMethodInfo(utilityMethod);
                aFloat newU = (aFloat) methodInfo.Invoke( aiSO, null );
                AddStatusNote(utilityMethod);
                return newU;
            } catch (Exception ex) {
                aiSO.personalLog.Log( $"<b>{aiScriptType}.{utilityMethod} CRASHED</b>" );
                Debug.LogError($"<b>{aiScriptType}.{utilityMethod} CRASHED</b>");
                AddStatusNote($"<b>{aiScriptType}.{utilityMethod} CRASHED</b>");
                return iAINode.defaultUtility;
            }
        }

        public eStatus InvokeExecute(string executeMethod) {
            if ( executeMethod == iAINode.nullMethodName ) return eStatus.failed;

            try {
                MethodInfo methodInfo = GetMethodInfo(executeMethod, Type.EmptyTypes);
                eStatus tStatus = (eStatus)methodInfo.Invoke(aiSO, null);
                AddStatusNote(executeMethod);
                return tStatus;
            } catch (Exception ex) {
                aiSO.personalLog.Log( $"<b>{aiScriptType}.{executeMethod} CRASHED</b>" );
                Debug.LogError($"<b>{aiScriptType}.{executeMethod} CRASHED</b>"  );
                AddStatusNote($"<b>{executeMethod} CRASHED</b>");
                return eStatus.failed;
            }
        }

        public aFloat InvokeUtilityDecoratorMethod( string decoratorMethod, aFloat utilityIn ) {
            if ( decoratorMethod[0] == iAINode.nullMethodChar ) return utilityIn;

            // Determine whether it is a Utility Decorator or a Utility method that does not take in parameters
            MethodRec mRec = GetMethodRec(decoratorMethod, new[] { typeof(aFloat) });
            // If the mRec is null, just pass through the utilityIn
            if ( mRec == null ) return utilityIn;

            aFloat newU;
            try {
                // The MethodRec can tell us which kind of Utility method it is (Decorator or not)
                if ( mRec.numParameters == 0 ) { // This is a standard Utility method 
                    newU = (aFloat) mRec.methodInfo.Invoke(aiSO, System.Array.Empty<object>());
                    AddStatusNote($"{decoratorMethod}()");
                    return newU;
                } else { // This is a Utility Decorator
                    newU = (aFloat) mRec.methodInfo.Invoke(aiSO, new object[] { utilityIn });
                    AddStatusNote($"{decoratorMethod}( {((float) utilityIn):0.00} )");
                    return newU;
                }
            } catch (Exception ex) {
                aiSO.personalLog.Log($"<b>{aiScriptType}.{decoratorMethod} CRASHED</b>");
                Debug.LogError($"<b>{aiScriptType}.{decoratorMethod} CRASHED</b>");
                AddStatusNote($"<b>{aiScriptType}.{decoratorMethod} CRASHED</b>");
                return utilityIn;
            }
            
            /* Old code before I created MethodRecs
            // Attempt it as a Utility Decorator Method
            System.Reflection.MethodInfo methodInfo = UnityEventBase.GetValidMethodInfo( aiSO, decoratorMethod, new Type[] {typeof(aFloat)} );
            if ( methodInfo != null ) {
                try {
                    return (aFloat) methodInfo.Invoke( aiSO, new object[] { utilityIn } );
                } catch (Exception ex) {
                    aiSO.personalLog.Log( ex.Message );
                    Debug.LogError(ex.Message  );
                    return utilityIn;
                }
            }
            // Attempt it as a standard Utility Method (with no aFloat parameter
            methodInfo = UnityEventBase.GetValidMethodInfo( aiSO, decoratorMethod, Type.EmptyTypes );
            if ( methodInfo != null ) {
                try {
                    return (aFloat) methodInfo.Invoke( aiSO, System.Array.Empty<object>() );
                } catch (Exception ex) {
                    aiSO.personalLog.Log( ex.Message );
                    Debug.LogError(ex.Message  );
                    return utilityIn;
                }
            }
            // Fallback to passthrough
            return utilityIn;
            */
        }
    }
#endregion


}
    







/* 
#region Editor Scripts
#if UNITY_EDITOR
    [CustomPropertyDrawer( typeof(AITree) )]
    public class AITree_Drawer : PropertyDrawer {
        // const float labelWidth = 32;



        // Draw the property inside the given rect
        public override void OnGUI( Rect position, SerializedProperty property, GUIContent label ) {
            // Get the object that the property belongs to
            object targetObject = property.serializedObject.targetObject;

            // Use reflection to get the value of the field from the object
            Type targetObjectType = targetObject.GetType();
            System.Reflection.FieldInfo fieldInfo = targetObjectType.GetField(property.name);
            object fieldValue = fieldInfo.GetValue(targetObject);
            AITree tree = fieldValue as AITree;
            AINode node = null;
            Type _nodeName = null;
            if ( tree != null ) {
                node = tree.child;
                _nodeName = node.GetType();
            }
            // The following line was difficult to find initially. - JGB
            // AINode node = fieldInfo.GetValue( property.serializedObject.targetObject ) as AINode;
            // AINode node = (AINode) property.objectReferenceValue;

            if ( node != null ) {
                // Type _nodeName = node.GetType();
                if ( _nodeName == typeof(AINode) ) {
                    EditorGUILayout.PropertyField( property, label, false );
                    return;
                }
                int edIndent = EditorGUI.indentLevel;
                EditorGUI.indentLevel = 0;

                // Using BeginProperty / EndProperty on the parent property means that
                // prefab override logic works on the entire property.
                EditorGUI.BeginProperty( position, label, property );
                {
                    if ( _nodeName == typeof(AINode_Composite) || _nodeName == typeof(AINode_Decorator) ) {
                        EditorGUILayout.BeginHorizontal();
                        {
                            GUILayout.Label( "Add:" );
                            if ( GUILayout.Button( "Leaf" ) ) { node.AddLeaf(); }
                            if ( GUILayout.Button( "Selector" ) ) { node.AddComposite(); }
                            if ( GUILayout.Button( "Decorator" ) ) { node.AddDecorator(); }
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.PropertyField( property, label, true );
                }
                EditorGUI.EndProperty();
                
                // // if ( Event.current.type == EventType.Repaint ) {
                // //     Debug.Log($"{Event.current.type} position: {position}"  );
                // // }
                // SerializedProperty nodeProp = property.FindPropertyRelative( "child" );
                //
                // // Using BeginProperty / EndProperty on the parent property means that
                // // prefab override logic works on the entire property.
                // EditorGUI.BeginProperty( position, label, nodeProp );
                // {
                //     if ( _nodeName == typeof(AINode_Composite) || _nodeName == typeof(AINode_Decorator) ) {
                //         EditorGUILayout.BeginHorizontal();
                //         {
                //             GUILayout.Label( "Add:" );
                //             if ( GUILayout.Button( "Leaf" ) ) { node.AddLeaf(); }
                //             if ( GUILayout.Button( "Selector" ) ) { node.AddComposite(); }
                //             if ( GUILayout.Button( "Decorator" ) ) { node.AddDecorator(); }
                //         }
                //         EditorGUILayout.EndHorizontal();
                //     }
                //     EditorGUILayout.PropertyField( nodeProp, label, true );
                // }
                // EditorGUI.EndProperty();

                EditorGUI.indentLevel = edIndent;
            }
        }
    }
#endif
#endregion
*/

    
    
