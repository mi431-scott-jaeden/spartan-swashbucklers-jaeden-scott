using UnityEngine;
using NaughtyAttributes;
using System.Collections.Generic;

namespace XnTools.XnAI {
    [System.Serializable]
    public class AINode_Decorator : iAINode {
        public enum eStatusModifier {none, invert, alwaysFail, alwaysSucceed, alwaysBusy};

        public enum eUtilityModifier { none, animationCurve, methodCall, xnCurveItem };


        // public string _nodeName = iAINode.nodeNameNotSet;
        // public override string nodeName {
        //     get { return _nodeName; }
        //     set { if (_nodeName == iAINode.nodeNameNotSet) _nodeName = value; }
        // }
        
#region Node Selection & SetAITreeAndParent
        
        // protected bool isPlaying => Application.isPlaying;
        //
        // [AllowNesting] [OnValueChanged( "SelectMe" )][HideIf( "isPlaying" )][Label("GetSelectNodeText")]
        // public bool thisNodeSelected = false;
        // protected void SelectMe() {
        //     if (!IsThisNodeSelected()) {
        //         thisNodeSelected = aiTree.SelectNode( this );
        //     } else {
        //         thisNodeSelected = false;
        //         aiTree.DeselectNode( this );
        //     }
        // }
        //
        // protected string GetSelectNodeText() {
        //     return ( IsThisNodeSelected() ) ? iAINode.nodeDeselectText : iAINode.nodeSelectText;
        // }
        //
        // public void UpdateThisNodeSelected() {
        //     thisNodeSelected = IsThisNodeSelected();
        // }
        //
        // protected bool IsThisNodeSelected() {
        //     if ( aiTree == null ) return false;
        //     iAINode selectedNode = aiTree.GetSelectedNode();
        //     if ( selectedNode == null ) return false;
        //     if ( selectedNode != this ) return false;
        //     return true;
        // }
        
        public override void SetAITreeAndParent( AITree tTree, iAINode tParent ) {
            base.SetAITreeAndParent(tTree, tParent);
            if ( childNode != null ) childNode.SetAITreeAndParent( tTree, this );
        }

#endregion
        
        
        [AllowNesting][SerializeField]//[ShowIf( "isPlaying" )]
        public eStatus _status = eStatus.failed;
        public override eStatus status { get => _status; protected set => _status = value; }

        [AllowNesting][SerializeField]//[ShowIf( "isPlaying" )]
        public float _utility = 0;
        public override aFloat utility { get => _utility; protected set => _utility = value._value; }
        
        // //[HideInInspector] 
        // protected AITree  aiTree;
        // public override AITree GetAITree() => aiTree;
        //
        // //[HideInInspector]
        // protected iAINode parent;
        // public override iAINode GetParent() => parent;

        
        
        public eStatusModifier statusModifier;
        public eUtilityModifier utilityModifier = eUtilityModifier.none;
        
        [SerializeField, SerializeReference][AllowNesting][ShowIf("utilityModifier", eUtilityModifier.methodCall)]
        [Dropdown( "GetUtilityMethods" )]
        // [OnValueChanged("AssignUtilityMethod")]
        public string utilityDecoratorMethod = iAINode.nullMethodName;

        protected List<string> GetUtilityMethods() {
            return aiTree.utilityDecoratorMethods;
            // return iAINode.GetMethodsByReturnType( this.GetType(), typeof(aFloat), "CheckUtility" );
        }

        [AllowNesting]
        [ShowIf( "utilityModifier", eUtilityModifier.xnCurveItem )]
        [Tooltip( "An XnInterpolation.CurveItem can perform simple mathematical interpolation easing." )]
        public XnInterpolation.CurveItem utilityXnCurveItem = new XnInterpolation.CurveItem();

        [SerializeReference][AllowNesting][ShowIf("utilityModifier", eUtilityModifier.animationCurve)]
        public AnimationCurve easingCurve;
        // public bool  utilityDecorator = false;
        // [HideIf( "utilityDecorator" )][AllowNesting]
        // [ShowIf( "utilityDecorator" )][AllowNesting]
        // public XnEasing utilityModifier;
        

        //[InfoBox("AINode_Decorator is not yet working to modify utility.")]
        [SerializeField, SerializeReference]
        [Label( "GetChildName" )][AllowNesting]
        public iAINode childNode;
        protected string GetChildName() => ( this.childNode == null ) ? "No Child assigned" : $"Child: {this.childNode.nodeName}";
        
        
        
        
#region Runtime Methods
        
        public override aFloat CheckUtility() {
            if ( utilityModifier == eUtilityModifier.methodCall ) {
                aiTree.currentNode = this;
                utility = aiTree.InvokeUtilityDecoratorMethod( utilityDecoratorMethod, utility );
                aiTree.currentNode = null;
                if ( utility > (aFloat) 0 ) return utility;
            }

            if ( childNode == null ) return 0;
            aFloat childUtility = childNode.CheckUtility();
            utility = childUtility;

            switch ( utilityModifier ) {
            case eUtilityModifier.methodCall:
                aiTree.currentNode = this;
                utility = aiTree.InvokeUtilityDecoratorMethod( utilityDecoratorMethod, utility );
                aiTree.currentNode = null;
                break;
            
            case eUtilityModifier.xnCurveItem:
                aiTree.currentNode = this;
                utility = utilityXnCurveItem.Evaluate( utility );
                statusNote = $"{utilityXnCurveItem.type}:{utilityXnCurveItem.num}";
                aiTree.currentNode = null;
                break;
            
            case eUtilityModifier.animationCurve:
                utility = easingCurve.Evaluate( utility );
                statusNote = $"AnimationCurve()";
                break;
            
            }
                
            return utility;
        }

        public override eStatus Execute(ref List<string> treeStatList, int callDepth) {
            int tSLCount = treeStatList.Count;
            eStatus childStatus = eStatus.failed; // This is the default status
            if ( childNode != null ) childStatus = childNode.Execute(ref treeStatList, callDepth+1);

            switch ( statusModifier ) {
            case eStatusModifier.invert:
                status = childStatus.Invert();
                return status;

            case eStatusModifier.alwaysFail:
                status = eStatus.failed;
                break;
            
            case eStatusModifier.alwaysSucceed:
                status = eStatus.success;
                break;
            
            case eStatusModifier.alwaysBusy:
                status = eStatus.busy;
                break;
            // In any other case, no modification will be made
            }

            treeStatList.Insert(tSLCount, GenerateStatus(callDepth));
            return status;
        }

#endregion
        
        

        // public  void AddLeaf() {
        //     childNode = new AINode_Leaf();
        //     childNode.SetAITreeAndParent( aiTree, this );
        // }
        //
        // public  void AddSelector() {
        //     childNode = new AINode_Composite();
        //     childNode.SetAITreeAndParent( aiTree, this );
        // }
        //
        // public  void AddDecorator() {
        //     childNode = new AINode_Decorator();
        //     childNode.SetAITreeAndParent( aiTree, this );
        // }
        
        public override void DeleteSelf() {
            bool success = false;
            if ( parent != null ) {
                success = parent.DeleteChildNode( this, childNode );
            }
        
            if ( success )
                Debug.Log( $"The node {_nodeName} was successfully removed." );
            else
                Debug.LogError( $"The node {_nodeName} FAILED to be removed." );
        }

        public override bool DeleteChildNode( iAINode node = null, iAINode replacement=null ) {
            if ( replacement != null ) {
                childNode = replacement;
                SetAITreeAndParent( aiTree, parent );
                return true;
            }
            if ( node != childNode ) return false; // This will also happen if node == null
            childNode = null;
            return true;
        }
        
        

        public override bool ReplaceChildNode( iAINode oldNode, iAINode newNode ) {
            if ( childNode == oldNode ) {
                childNode = newNode;
                return true;
            }
            return false;
        }

        public override bool AddChildNode( iAINode node ) {
            if ( node == null ) return false;
            childNode = node;
            return true;
        }

    }
}
